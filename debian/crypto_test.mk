include debian/sources.mk

NAME = crypto_test

SOURCES = $(crypto_test_sources)
OBJECTS = $(SOURCES:.cc=.o)

# src/crypto/pkcs8/pkcs12_test.cc:37:19: error: ISO C++20 does not permit initialization of char array with UTF-8 string literal
CXXFLAGS += -std=gnu++2a
CPPFLAGS += \
  -Isrc/include \

LDFLAGS += \
  -Ldebian/out \
  -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -lcrypto \
  -lgtest \
  -lpthread \
  -lssl \
  -ltest_support \
  -pie

ifneq ($(filter mipsel mips64el,$(DEB_HOST_ARCH)),)
  LDFLAGS += -Wl,-z,notext
endif

# -latomic should be the last library specified
# https://github.com/android/ndk/issues/589
ifeq ($(DEB_HOST_ARCH), armel)
  LDFLAGS += -latomic
endif

# clang built crypto_test binary crashes on mips64el
# so fallback to gcc as workaround
ifeq ($(DEB_HOST_ARCH), mips64el)
  CXX = g++
endif

build: $(OBJECTS)
	$(CXX) $^ -o debian/out/$(NAME) $(LDFLAGS)

$(OBJECTS): %.o: %.cc
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)
