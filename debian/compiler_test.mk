NAME = compiler_test

SOURCES = \
  src/crypto/compiler_test.cc \
  src/crypto/test/test_util.cc \

OBJECTS = $(SOURCES:.cc=.o)

CXXFLAGS += -std=gnu++2a
CPPFLAGS += \
  -Isrc/include \

LDFLAGS += \
  -lgtest \
  -lpthread \
  -pie

ifneq ($(filter mipsel mips64el,$(DEB_HOST_ARCH)),)
  LDFLAGS += -Wl,-z,notext
endif

build: $(OBJECTS) /usr/lib/$(DEB_HOST_MULTIARCH)/libgtest_main.a
	mkdir -p debian/out
	$(CXX) $^ -o debian/out/$(NAME) $(LDFLAGS)

$(OBJECTS): %.o: %.cc
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)
